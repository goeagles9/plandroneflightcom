//
//  TimelineMissionViewController.swift
//  FlightPlanning
//
//  Created by Richard Demeny on 02/04/2017.
//  Copyright © 2017 Nimbus. All rights reserved.
//

import UIKit
import DJISDK
import GoogleMaps
import GooglePlaces
import MapKit
import Swift
import CoreData
import MessageUI
import Foundation

enum TimelineElementKind: String {
    case takeOff = "Take Off"
    case goTo = "Go To"
    case goHome = "Go Home"
    case gimbalAttitude = "Gimbal Attitude"
    case singleShootPhoto = "Single Photo"
    case continuousShootPhoto = "Continuous Photo"
    case recordVideoDuration = "Record Duration"
    case recordVideoStart = "Start Record"
    case recordVideoStop = "Stop Record"
    case waypointMission = "Waypoint Mission"
    case hotpointMission = "Hotpoint Mission"
}

class TimelineMissionViewController: UIViewController, UICollectionViewDelegate, GMSMapViewDelegate, UICollectionViewDataSource, UIAlertViewDelegate, UITableViewDataSource, NSFetchedResultsControllerDelegate, GMSAutocompleteViewControllerDelegate, MFMailComposeViewControllerDelegate {
    @IBOutlet var mapView: GMSMapView!
    var markersArray: Array<CLLocationCoordinate2D> = []
    var mapName: String!
    @IBOutlet var tableForMaps: UITableView!
    @IBOutlet var DeleteAll: UIButton!
    
    @IBOutlet weak var availableElementsView: UICollectionView!
    var availableElements = [TimelineElementKind]()

    var homeAnnotation = DJIImageAnnotation(identifier: "homeAnnotation")
    var aircraftAnnotation = DJIImageAnnotation(identifier: "aircraftAnnotation")
    
    var aircraftAnnotationView: MKAnnotationView!
    
    @IBOutlet weak var timelineView: UICollectionView!
    var scheduledElements = [TimelineElementKind]()
    
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var stopButton: UIButton!
    
    @IBOutlet weak var simulatorButton: UIButton!
    
    fileprivate var _isSimulatorActive: Bool = false

    weak var appDelegate: AppDelegate! = UIApplication.shared.delegate as? AppDelegate
    
    public var isSimulatorActive: Bool {
        get {
            return _isSimulatorActive
        }
        set {
            _isSimulatorActive = newValue
            self.simulatorButton.titleLabel?.text = _isSimulatorActive ? "Stop Simulator" : "Start Simulator"
        }
    }
    
    // MARK: - Functions
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        guard editingStyle == .delete else { return }
        
        // Fetch Map
        let map = fetchedResultsControllerForMapEntity.object(at: indexPath)
        
        // Delete Map
        managedObjectContext.delete(map)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let mapnames = fetchedResultsControllerForMapEntity.fetchedObjects else { return 0 }
        return mapnames.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: MapsTableViewCell.reuseIdentifier, for: indexPath) as? MapsTableViewCell else {
            fatalError("Unexpected Index Path")
        }
        
        let map = fetchedResultsControllerForMapEntity.object(at: indexPath)
        
        // Configure Cell
        cell.mapNameLabel.text = map.name
        cell.loadMapButton.accessibilityHint = map.name
        cell.deleteMapButton.accessibilityHint = map.name
        cell.sendMapButton.accessibilityHint = map.name
        cell.loadMapButton.addTarget(self, action: #selector(loadSavedMapView), for: .touchUpInside)
        cell.deleteMapButton.addTarget(self, action: #selector(deleteOneMap), for: .touchUpInside)
        cell.sendMapButton.addTarget(self, action: #selector(sendButtonClicked), for: .touchUpInside)
        return cell
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch (type) {
        case .insert:
            if let indexPath = newIndexPath {
                tableForMaps.insertRows(at: [indexPath], with: .fade)
            }
            break;
        case .delete:
            if let indexPath = indexPath {
                tableForMaps.deleteRows(at: [indexPath], with: .fade)
            }
            break;
        case .update:
            if let indexPath = indexPath {
                guard let cell = tableForMaps.dequeueReusableCell(withIdentifier: MapsTableViewCell.reuseIdentifier, for: indexPath) as? MapsTableViewCell else {
                    fatalError("Unexpected Index Path")
                }
                let map = fetchedResultsControllerForMapEntity.object(at: indexPath)
                
                // Configure Cell
                cell.mapNameLabel.text = map.name
                cell.loadMapButton.accessibilityHint = map.name
            }
            
            break;
        case .move:
            if let indexPath = indexPath {
                tableForMaps.deleteRows(at: [indexPath], with: .fade)
            }
            
            if let newIndexPath = newIndexPath {
                tableForMaps.insertRows(at: [newIndexPath], with: .fade)
            }
            break;
        }
    }
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<Map>) {
        tableForMaps.beginUpdates()
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<Map>) {
        tableForMaps.endUpdates()
    }
    
    
    
//    func whatever {
//    //fetch with mapname
//    
//    }
    
    
    
    
    func loadSavedMapView(_ sender: UIButton) -> UIButton {
        mapView.clear()
        self.markersArray.removeAll()
        var mapname = sender.accessibilityHint
        fetchedResultsControllerForCoordinateEntity.fetchRequest.predicate = NSPredicate(format: "map.name == %@", mapname!)
        
        do {
            try fetchedResultsControllerForCoordinateEntity.performFetch()
        } catch {
            let fetchError = error as NSError
            print("Unable to Perform Fetch Request")
            print("\(fetchError), \(fetchError.localizedDescription)")
        }
        guard let mapCoordinates = fetchedResultsControllerForCoordinateEntity.fetchedObjects else { return sender }
        
        for coordinate in mapCoordinates {
            var lat: CLLocationDegrees = coordinate.latitude
            var lng: CLLocationDegrees = coordinate.longitude
            var formattedCoordinate = CLLocationCoordinate2DMake(lat,lng)
            markersArray.append(formattedCoordinate)
        }
        
        for coordinate in markersArray {
            let marker1 = GMSMarker()
            marker1.position = coordinate
            marker1.map = mapView
            marker1.isDraggable = true
            marker1.icon = GMSMarker.markerImage(with: UIColor.blue)
        }
        
        let path = GMSMutablePath()
        var i = 0
        
        for coordinate in markersArray {
            path.add(coordinate)
            i = i+1
        }
        let polygon = GMSPolygon(path:path)
        polygon.map = nil;//not sure if this line is redundant
        polygon.fillColor = UIColor.green
        polygon.strokeColor = UIColor.black
        polygon.strokeWidth = 1
        polygon.map = mapView
        
        var camera = GMSCameraPosition.camera(withLatitude: markersArray[0].latitude, longitude: markersArray[0].longitude, zoom: 10)
        mapView.camera = camera
        return sender
    }

    
    @IBAction func loadMapsButtonClicked(_ sender: Any) {
        tableForMaps.isHidden = !(tableForMaps).isHidden
        DeleteAll.isHidden = !(DeleteAll).isHidden
    }
    
    @IBAction func goToLocationButtonClicked(_ sender: Any) {
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        // Set a filter to return only addresses.
        let filter = GMSAutocompleteFilter()
        filter.type = .address
        autocompleteController.autocompleteFilter = filter
        present(autocompleteController, animated: true, completion: nil)
    }
    
    @IBAction func saveMapButtonClicked(_ sender: Any) {
        let alert = UIAlertController(title: "Save Map", message: "Please enter a name for your map:", preferredStyle: .alert)
        
        let saveAction = UIAlertAction(title: "Save", style: .default) {
            [unowned self] action in
            guard let textField = alert.textFields?.first,
                let mapName = textField.text else {
                    return
            }
            
            var newMap = NSEntityDescription.insertNewObject(forEntityName: "Map", into: managedObjectContext)
            newMap.setValue(mapName, forKey: "name")
            
            if self.markersArray.count > 2 {
                for member in self.markersArray {
                    var theCoordinate = NSEntityDescription.insertNewObject(forEntityName: "Coordinate", into: managedObjectContext)
                    theCoordinate.setValue(member.latitude, forKey: "latitude")
                    theCoordinate.setValue(member.longitude, forKey: "longitude")
                    theCoordinate.setValue(newMap, forKey: "map")
                }} else {
                return
            }
            
            do {
                try self.appDelegate.saveContext()
            }
            catch {
                //put catch here
            }
            //remove previous markers + polygons, reset markersArray
            self.mapView.clear()
            self.markersArray.removeAll()
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default)
        
        alert.addTextField()
        
        alert.addAction(saveAction)
        alert.addAction(cancelAction)
        
        present(alert, animated: true)
    }

    @IBAction func sendButtonClicked(_ sender: Any) {
        let alert = UIAlertController(title: "Send Waypoints as CSV", message: "Please enter your e-mail address below:", preferredStyle: .alert)
        
        let sendAction = UIAlertAction(title: "Send", style: .default) {
        //get user's e-mail address
        [unowned self] action in
        guard let textField = alert.textFields?.first, let emailAddress = textField.text
        else {return}
                                        
        //get the map name of tapped row
        var mapname = (sender as AnyObject).accessibilityHint
                                        
        //filter amongst Map objects and try fetch
        self.fetchedResultsControllerForCoordinateEntity.fetchRequest.predicate = NSPredicate(format: "map.name == %@", mapname!!)
                                        
        do {
        try self.fetchedResultsControllerForCoordinateEntity.performFetch()
        }
        catch {
        let fetchError = error as NSError
        print("Unable to Perform Fetch Request")
        print("\(fetchError), \(fetchError.localizedDescription)")
        }
                                        
        //get data
        guard let mapCoordinates = self.fetchedResultsControllerForCoordinateEntity.fetchedObjects else { return }
                                        
        //init string array to hold latitude and longitude values
        var fetchedCoordinates: Array<String> = []
                                        
        //loop through every coordinate and store them
        for data in mapCoordinates {
        //convert to comma-separated values
        var lat = "latitude: " + String(data.latitude)
        var lng = "longitude: " + String(data.longitude)
        var coordinate = lat + ", " + lng
        fetchedCoordinates.append(coordinate)
        }
                                        
        //send to e-mail
            
            let mailComposeViewController = self.configuredMailComposeViewController()
            if MFMailComposeViewController.canSendMail() {
                self.present(mailComposeViewController, animated: true, completion: nil)
            } else {
                self.showSendMailErrorAlert()
            }
                                        
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default)
        
        alert.addTextField()
        
        alert.addAction(sendAction)
        alert.addAction(cancelAction)
        
        present(alert, animated: true)
    }
    
    func configuredMailComposeViewController() -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self // Extremely important to set the --mailComposeDelegate-- property, NOT the --delegate-- property
        mailComposerVC.setToRecipients(["nurdin@gmail.com"])
        mailComposerVC.setSubject("Sending you an in-app e-mail...")
        mailComposerVC.setMessageBody("Sending e-mail in-app is not so bad!", isHTML: false)
        
        return mailComposerVC
    }
    
    func showSendMailErrorAlert() {
        let sendMailErrorAlert = UIAlertView(title: "Could Not Send Email", message: "Your device could not send e-mail.  Please check e-mail configuration and try again.", delegate: self, cancelButtonTitle: "OK")
        sendMailErrorAlert.show()
    }
    
    // MARK: MFMailComposeViewControllerDelegate
    
    func mailComposeController(controller: MFMailComposeViewController!, didFinishWithResult result: MFMailComposeResult, error: NSError!) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    func deleteOneMap(_ sender: UIButton) {
        var mapname = sender.accessibilityHint
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Map")
        fetchRequest.predicate = NSPredicate(format: "name == %@", mapname!)

        if let result = try? managedObjectContext.fetch(fetchRequest) {
            for object in result {
                managedObjectContext.delete(object as! NSManagedObject)
            }
        }
    }
    
    @IBAction func deleteAllMaps(_ sender: Any) {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Map")
        
        if let result = try? managedObjectContext.fetch(fetchRequest) {
            for object in result {
                managedObjectContext.delete(object as! NSManagedObject)
            }
        }
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D){
        var lat: CLLocationDegrees = coordinate.latitude
        var lng: CLLocationDegrees = coordinate.longitude
        var formattedCoordinate = CLLocationCoordinate2DMake(lat, lng)
        markersArray.append(formattedCoordinate)
        mapView.clear()
        addMarker()
        drawPolygon()
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        var lat: CLLocationDegrees = marker.position.latitude
        var lng: CLLocationDegrees = marker.position.longitude
        var formattedCoordinate = CLLocationCoordinate2DMake(lat, lng)
        markersArray = markersArray.filter({ !(($0.latitude == formattedCoordinate.latitude) && ($0.longitude == formattedCoordinate.longitude)) })
        mapView.clear()
        drawPolygon()
        addMarker()
        return true
    }
    
    func mapView(_ mapView: GMSMapView, didBeginDragging marker: GMSMarker){
        var lat: CLLocationDegrees = marker.position.latitude
        var lng: CLLocationDegrees = marker.position.longitude
        var formattedCoordinate = CLLocationCoordinate2DMake(lat, lng)
        markersArray = markersArray.filter({ !(($0.latitude == formattedCoordinate.latitude) && ($0.longitude == formattedCoordinate.longitude))})
    }
    
    func mapView(_ mapView: GMSMapView, didDrag marker: GMSMarker){
        var lat: CLLocationDegrees = marker.position.latitude
        var lng: CLLocationDegrees = marker.position.longitude
        var formattedCoordinate = CLLocationCoordinate2DMake(lat, lng)
        markersArray.append(formattedCoordinate)
        mapView.clear()
        drawPolygon()
        addMarker()
        markersArray = markersArray.filter({ !(($0.latitude == formattedCoordinate.latitude) && ($0.longitude == formattedCoordinate.longitude))})
        }
    
    func mapView(_ mapView: GMSMapView, didEndDragging marker: GMSMarker){
        var lat: CLLocationDegrees = marker.position.latitude
        var lng: CLLocationDegrees = marker.position.longitude
        var formattedCoordinate = CLLocationCoordinate2DMake(lat, lng)
        markersArray.append(formattedCoordinate)
        mapView.clear()
        drawPolygon()
        addMarker()
        }
    
    func addMarker(){
        for coordinate in markersArray {
            let marker = GMSMarker()
            marker.position = coordinate
            marker.map = mapView
            marker.isDraggable = true
            marker.icon = GMSMarker.markerImage(with: UIColor.blue)
        }
    }
    
    func drawPolygon(){
        let path = GMSMutablePath()
        var i = 0
        // get path for polygon
        for coordinate in markersArray {
            path.add(coordinate)
            i = i+1
        }
        // build polygon
        let polygon = GMSPolygon(path:path)
        polygon.map = nil;//not sure if this line is redundant
        polygon.fillColor = UIColor.green
        polygon.strokeColor = UIColor.black
        polygon.strokeWidth = 1
        polygon.map = mapView
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        DJISDKManager.missionControl()?.addListener(self, toTimelineProgressWith: { (event: DJIMissionControlTimelineEvent, element: DJIMissionControlTimelineElement?, error: Error?, info: Any?) in
            
            switch event {
            case .started:
                self.didStart()
            case .stopped:
                self.didStop()
            case .paused:
                self.didPause()
            case .resumed:
                self.didResume()
            default:
                break
            }
        })
        
//        self.mapView.addAnnotations([self.aircraftAnnotation, self.homeAnnotation])
        
        if let aircarftLocationKey = DJIFlightControllerKey(param: DJIFlightControllerParamAircraftLocation)  {
            DJISDKManager.keyManager()?.startListeningForChanges(on: aircarftLocationKey, withListener: self) { [unowned self] (oldValue: DJIKeyedValue?, newValue: DJIKeyedValue?) in
                if newValue != nil {
                    let newLocationValue = newValue!.value as! DJISDKLocation
                    
                    if CLLocationCoordinate2DIsValid(newLocationValue.coordinate) {
                        self.aircraftAnnotation.coordinate = newLocationValue.coordinate
                    }
                }
            }
        }
        
        
        if let aircraftHeadingKey = DJIFlightControllerKey(param: DJIFlightControllerParamCompassHeading) {
            DJISDKManager.keyManager()?.startListeningForChanges(on: aircraftHeadingKey, withListener: self) { [unowned self] (oldValue: DJIKeyedValue?, newValue: DJIKeyedValue?) in
                if (newValue != nil) {
                    self.aircraftAnnotation.heading = newValue!.doubleValue
                    if (self.aircraftAnnotationView != nil) {
                        self.aircraftAnnotationView.transform = CGAffineTransform(rotationAngle: CGFloat(self.degreesToRadians(Double(self.aircraftAnnotation.heading))))
                    }
                }
            }
        }
        
        if let homeLocationKey = DJIFlightControllerKey(param: DJIFlightControllerParamHomeLocation) {
            DJISDKManager.keyManager()?.startListeningForChanges(on: homeLocationKey, withListener: self) { [unowned self] (oldValue: DJIKeyedValue?, newValue: DJIKeyedValue?) in
                if (newValue != nil) {
                    let newLocationValue = newValue!.value as! DJISDKLocation
                    
                    if CLLocationCoordinate2DIsValid(newLocationValue.coordinate) {
                        self.homeAnnotation.coordinate = newLocationValue.coordinate
                    }
                }
            }
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        DJISDKManager.missionControl()?.removeListener(self)
        DJISDKManager.keyManager()?.stopAllListening(ofListeners: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        DeleteAll.isHidden = true
        tableForMaps.isHidden = true
        
        do {
            try fetchedResultsControllerForMapEntity.performFetch()
        } catch {
            let fetchError = error as NSError
            print("Unable to Perform Fetch Request")
            print("\(fetchError), \(fetchError.localizedDescription)")
        }
        
        self.availableElementsView.delegate = self
        self.availableElementsView.dataSource = self
        
        self.timelineView.delegate = self
        self.timelineView.dataSource = self
        
        self.availableElements.append(contentsOf: [.takeOff, .goTo, .goHome, .gimbalAttitude, .singleShootPhoto, .continuousShootPhoto, .recordVideoDuration, .recordVideoStart, .recordVideoStop, .waypointMission, .hotpointMission])
        
        //set default position of the mapView
        var camera = GMSCameraPosition.camera(withLatitude: 60.1128, longitude: 13.996, zoom: 10)
        //mapView.mapType = hybrid
        mapView.isMyLocationEnabled = true
        mapView.settings.compassButton = true
        mapView.settings.myLocationButton = true
        mapView.camera = camera
        mapView.delegate = self
        
        if let isSimulatorActiveKey = DJIFlightControllerKey(param: DJIFlightControllerParamIsSimulatorActive) {
            DJISDKManager.keyManager()?.startListeningForChanges(on: isSimulatorActiveKey, withListener: self, andUpdate: { (oldValue: DJIKeyedValue?, newValue : DJIKeyedValue?) in
                if newValue?.boolValue != nil {
                    self.isSimulatorActive = (newValue?.boolValue)!
                }
            })
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    // MARK: - MKMapViewDelegate
    
//    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
//        var image: UIImage!
//        
//        if annotation.isEqual(self.aircraftAnnotation) {
//            image = #imageLiteral(resourceName: "aircraft")
//        } else if annotation.isEqual(self.homeAnnotation) {
//            image = #imageLiteral(resourceName: "navigation_poi_pin")
//        }
//        
//        let imageAnnotation = annotation as! DJIImageAnnotation
//        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: imageAnnotation.identifier)
//        
//        if annotationView == nil {
//            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: imageAnnotation.identifier)
//        }
//        
//        annotationView?.image = image
//        
//        if annotation.isEqual(self.aircraftAnnotation) {
//            if annotationView != nil {
//                self.aircraftAnnotationView = annotationView!
//            }
//        }
//        
//        return annotationView
//    }
    
    
    fileprivate var started = false
    fileprivate var paused = false
    
    @IBAction func playButtonAction(_ sender: Any) {
        if self.paused {
            DJISDKManager.missionControl()?.resumeTimeline()
        } else if self.started {
            DJISDKManager.missionControl()?.pauseTimeline()
        } else {
            DJISDKManager.missionControl()?.startTimeline()
        }
    }
    
    @IBAction func stopButtonAction(_ sender: Any) {
        DJISDKManager.missionControl()?.stopTimeline()
    }
    
    @IBAction func startSimulatorButtonAction(_ sender: Any) {
        guard let droneLocationKey = DJIFlightControllerKey(param: DJIFlightControllerParamAircraftLocation) else {
            return
        }
        
        guard let droneLocationValue = DJISDKManager.keyManager()?.getValueFor(droneLocationKey) else {
            return
        }
        
        let droneLocation = droneLocationValue.value as! DJISDKLocation
        let droneCoordinates = droneLocation.coordinate
        
        if let aircraft = DJISDKManager.product() as? DJIAircraft {
            if self.isSimulatorActive {
                aircraft.flightController?.simulator?.stop(completion: nil)
            } else {
                aircraft.flightController?.simulator?.start(withLocation: droneCoordinates,
                                                            updateFrequency: 30,
                                                            gpsSatellitesNumber: 12,
                                                            withCompletion: { (error) in
                                                                if (error != nil) {
                                                                    NSLog("Start Simulator Error: \(error.debugDescription)")
                                                                }
                })
            }
        }
    }
    
    func didStart() {
        self.started = true
        DispatchQueue.main.async {
            self.stopButton.isEnabled = true
            self.playButton.setTitle("⏸", for: .normal)
        }
    }
    
    func didPause() {
        self.paused = true
        DispatchQueue.main.async {
            self.playButton.setTitle("▶️", for: .normal)
        }
    }
    
    func didResume() {
        self.paused = false
        DispatchQueue.main.async {
            self.playButton.setTitle("⏸", for: .normal)
        }
    }
    
    func didStop() {
        self.started = false
        DispatchQueue.main.async {
            self.stopButton.isEnabled = false
            self.playButton.setTitle("▶️", for: .normal)
        }
    }
    
    //MARK: OutlineView Delegate & Datasource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.availableElementsView {
            return self.availableElements.count
        } else if collectionView == self.timelineView {
            return self.scheduledElements.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "elementCell", for: indexPath) as! TimelineElementCollectionViewCell
        
        if collectionView == self.availableElementsView {
            cell.label.text = self.availableElements[indexPath.row].rawValue
        } else if collectionView == self.timelineView {
            cell.label.text = self.scheduledElements[indexPath.row].rawValue
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView.isEqual(self.availableElementsView) {
            let elementKind = self.availableElements[indexPath.row]
            
            guard let element = self.timelineElementForKind(kind: elementKind) else {
                return;
            }
            let error = DJISDKManager.missionControl()?.scheduleElement(element)
            
            if error != nil {
                NSLog("Error scheduling element \(error)")
                return;
            }
            
            self.scheduledElements.append(elementKind)
            DispatchQueue.main.async {
                self.timelineView.reloadData()
            }
        } else if collectionView.isEqual(self.timelineView) {
            if self.started == false {
                DJISDKManager.missionControl()?.unscheduleElement(at: UInt(indexPath.row))
                self.scheduledElements.remove(at: indexPath.row)
                DispatchQueue.main.async {
                    self.timelineView.reloadData()
                }
            }
        }
    }
    
    // MARK : Timeline Element
    
    func timelineElementForKind(kind: TimelineElementKind) -> DJIMissionControlTimelineElement? {
        switch kind {
        case .takeOff:
            return DJITakeOffAction()
        case .goTo:
            return DJIGoToAction(altitude: 30)
        case .goHome:
            return DJIGoHomeAction()
        case .gimbalAttitude:
            return self.defaultGimbalAttitudeAction()
        case .singleShootPhoto:
            return DJIShootPhotoAction(singleShootPhoto: ())
        case .continuousShootPhoto:
            return DJIShootPhotoAction(photoCount: 10, timeInterval: 3.0)
        case .recordVideoDuration:
            return DJIRecordVideoAction(duration: 10)
        case .recordVideoStart:
            return DJIRecordVideoAction(startRecordVideo: ())
        case .recordVideoStop:
            return DJIRecordVideoAction(stopRecordVideo: ())
        case .waypointMission:
            return self.defaultWaypointMission()
        case .hotpointMission:
            return self.defaultHotPointAction()
        }
    }
    
    
    func defaultGimbalAttitudeAction() -> DJIGimbalAttitudeAction? {
        let attitude = DJIGimbalAttitude(pitch: 30.0, roll: 0.0, yaw: 0.0)
        
        return DJIGimbalAttitudeAction(attitude: attitude)
    }
    
    func defaultWaypointMission() -> DJIWaypointMission? {
        let mission = DJIMutableWaypointMission()
        mission.maxFlightSpeed = 15
        mission.autoFlightSpeed = 8
        mission.finishedAction = .noAction
        mission.headingMode = .auto
        mission.flightPathMode = .normal
        mission.rotateGimbalPitch = true
        mission.exitMissionOnRCSignalLost = true
        mission.gotoFirstWaypointMode = .pointToPoint
        mission.repeatTimes = 1
        
        guard let droneLocationKey = DJIFlightControllerKey(param: DJIFlightControllerParamAircraftLocation) else {
            return nil
        }
        
        guard let droneLocationValue = DJISDKManager.keyManager()?.getValueFor(droneLocationKey) else {
            return nil
        }
        
        let droneLocation = droneLocationValue.value as! DJISDKLocation
        let droneCoordinates = droneLocation.coordinate
        
        if !CLLocationCoordinate2DIsValid(droneCoordinates) {
            return nil
        }
        
        mission.pointOfInterest = droneCoordinates
        let offset = 0.0000899322
        
        let loc1 = CLLocationCoordinate2DMake(droneCoordinates.latitude + offset, droneCoordinates.longitude)
        let waypoint1 = DJIWaypoint(coordinate: loc1)
        waypoint1.altitude = 25
        waypoint1.heading = 0
        waypoint1.actionRepeatTimes = 1
        waypoint1.actionTimeoutInSeconds = 60
        waypoint1.cornerRadiusInMeters = 5
        waypoint1.turnMode = .clockwise
        waypoint1.gimbalPitch = 0
        
        let loc2 = CLLocationCoordinate2DMake(droneCoordinates.latitude, droneCoordinates.longitude + offset)
        let waypoint2 = DJIWaypoint(coordinate: loc2)
        waypoint2.altitude = 26
        waypoint2.heading = 0
        waypoint2.actionRepeatTimes = 1
        waypoint2.actionTimeoutInSeconds = 60
        waypoint2.cornerRadiusInMeters = 5
        waypoint2.turnMode = .clockwise
        waypoint2.gimbalPitch = -90
        
        let loc3 = CLLocationCoordinate2DMake(droneCoordinates.latitude - offset, droneCoordinates.longitude)
        let waypoint3 = DJIWaypoint(coordinate: loc3)
        waypoint3.altitude = 27
        waypoint3.heading = 0
        waypoint3.actionRepeatTimes = 1
        waypoint3.actionTimeoutInSeconds = 60
        waypoint3.cornerRadiusInMeters = 5
        waypoint3.turnMode = .clockwise
        waypoint3.gimbalPitch = 0
        
        let loc4 = CLLocationCoordinate2DMake(droneCoordinates.latitude, droneCoordinates.longitude - offset)
        let waypoint4 = DJIWaypoint(coordinate: loc4)
        waypoint4.altitude = 28
        waypoint4.heading = 0
        waypoint4.actionRepeatTimes = 1
        waypoint4.actionTimeoutInSeconds = 60
        waypoint4.cornerRadiusInMeters = 5
        waypoint4.turnMode = .clockwise
        waypoint4.gimbalPitch = -90
        
        let waypoint5 = DJIWaypoint(coordinate: loc1)
        waypoint5.altitude = 29
        waypoint5.heading = 0
        waypoint5.actionRepeatTimes = 1
        waypoint5.actionTimeoutInSeconds = 60
        waypoint5.cornerRadiusInMeters = 5
        waypoint5.turnMode = .clockwise
        waypoint5.gimbalPitch = 0
        
        mission.add(waypoint1)
        mission.add(waypoint2)
        mission.add(waypoint3)
        mission.add(waypoint4)
        mission.add(waypoint5)
        
        return DJIWaypointMission(mission: mission)
    }
    
    func defaultHotPointAction() -> DJIHotpointAction? {
        let mission = DJIHotpointMission()
        
        guard let droneLocationKey = DJIFlightControllerKey(param: DJIFlightControllerParamAircraftLocation) else {
            return nil
        }
        
        guard let droneLocationValue = DJISDKManager.keyManager()?.getValueFor(droneLocationKey) else {
            return nil
        }
        
        let droneLocation = droneLocationValue.value as! DJISDKLocation
        let droneCoordinates = droneLocation.coordinate
        
        if !CLLocationCoordinate2DIsValid(droneCoordinates) {
            return nil
        }
        
        let offset = 0.0000899322
        
        mission.hotpoint = CLLocationCoordinate2DMake(droneCoordinates.latitude + offset, droneCoordinates.longitude)
        mission.altitude = 15
        mission.radius = 15
        mission.angularVelocity = DJIHotpointMissionOperator.maxAngularVelocity(forRadius: mission.radius) * (arc4random() % 2 == 0 ? -1 : 1)
        mission.startPoint = .nearest
        mission.heading = .alongCircleLookingForward
        
        return DJIHotpointAction(mission: mission, surroundingAngle: 180)
    }
    
    // MARK: - Convenience
    
    func degreesToRadians(_ degrees: Double) -> Double {
        return M_PI / 180 * degrees
    }
    
    // MARK: - Controllers for Core Data

    lazy var fetchedResultsControllerForMapEntity: NSFetchedResultsController<Map> = {
        let fetchRequest: NSFetchRequest<Map> = Map.fetchRequest()
        
        var primarySortDescriptor = NSSortDescriptor(key: "name", ascending: true)
        fetchRequest.sortDescriptors = [primarySortDescriptor]
        
        let fetchedResultsControllerForMapEntity = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: managedObjectContext, sectionNameKeyPath: nil, cacheName: nil)
        
        fetchedResultsControllerForMapEntity.delegate = self
        
        return fetchedResultsControllerForMapEntity
        
    }()
    
    lazy var fetchedResultsControllerForCoordinateEntity: NSFetchedResultsController<Coordinate> = {
        let fetchRequest: NSFetchRequest<Coordinate> = Coordinate.fetchRequest()
        
        let primarySortDescriptor = NSSortDescriptor(key: "latitude", ascending: true)
        let secondarySortDescriptor = NSSortDescriptor(key: "longitude", ascending: true)
        fetchRequest.sortDescriptors = [primarySortDescriptor, secondarySortDescriptor]
        
        let fetchedResultsControllerForCoordinateEntity = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: managedObjectContext, sectionNameKeyPath: nil, cacheName: nil)
        fetchedResultsControllerForCoordinateEntity.delegate = self
        return fetchedResultsControllerForCoordinateEntity
    }()
    
    // MARK: Google Search

    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        var chosenLocation = place.coordinate
        var camera = GMSCameraPosition.camera(withLatitude: place.coordinate.latitude, longitude: place.coordinate.longitude, zoom: 10)
        mapView.camera = camera
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print("Error: ", error.localizedDescription)
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Show the network activity indicator.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    // Hide the network activity indicator.
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }

}
