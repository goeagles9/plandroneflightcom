//
//  RegisterLogin.swift
//  FlightPlanning
//
//  Created by Richard Demeny on 21/04/2017.
//  Copyright © 2017 Nimbus. All rights reserved.
//

import UIKit

class Register: UIViewController {
    @IBOutlet var usernameTextField: UITextField!
    @IBOutlet var passwordTextField: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    
    // MARK: - User Registration / Login
    
    @IBAction func GoBackToLoginButtonClicked(_ sender: Any) {
        self.performSegue(withIdentifier: "GoBackToLogin", sender: nil)
    }
    
    @IBAction func RegisterButtonClicked(_ sender: Any) {
        var user = PFUser()
        user.username = usernameTextField.text
        user.password = passwordTextField.text
//        user.email = emlEntered
        user.signUpInBackground {
            (succeeded, error) -> Void in
            if error == nil {
                // Hooray! Let them use the app now.
                print(succeeded)
            } else {
                // Show the errorString somewhere and let the user try again.
                print(error)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
