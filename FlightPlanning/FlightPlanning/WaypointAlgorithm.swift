//
//  WaypointAlgorithm.swift
//  FlightPlanning
//
//  Created by James Livingston Kaye on 4/10/17.
//  Copyright © 2017 Nimbus. All rights reserved.
//
//
import MapKit
import Foundation

class WaypointCalculation{
    let converter: GeodeticUTMConverter = GeodeticUTMConverter()

    func onedroneWP (polylist: Array<CLLocationCoordinate2D>, sidelap: Double,polydegreeUser: Double, altitude: Double) -> Array<CLLocationCoordinate2D> {
            
        var wpalgorithmlist = Array<CLLocationCoordinate2D>()
        
        //Convert polylist input (arraylist LatLng) to UTM (arraylist LatLng)
        let polylistUTM = wsgListToUTM(points: polylist)
        
        let pos2 = converter.latitudeAndLongitude(toUTMCoordinates: polylist[0])
        let zoner = pos2.gridZone
        let lett = pos2.hemisphere
        
        //Convert polylistUTM ArrayList<Double> to double
        let points = polylistUTM
        
        //Check that area isn't too small.
        let phantom3lens = 1.71111111122;
        let width = altitude*(phantom3lens);
        let shift = shiftfun(width: width, sidelap: sidelap);
        // Determine user slope from degrees input.
        let slopeUser = tan(toRadians(degrees: polydegreeUser));
        
        // i. Determine slope perpendicular to slopeUser
        let slopeperp = tan(toRadians(degrees: polydegreeUser+90));
        
        // ii. Determine intersection points made from boundary points
        // with slopeUser and a line with slope perpendicular to slope User.
        var xperp = Array<Double>();
        var yperp = Array<Double>();
        for (_, c) in points.enumerated() {
            xperp.append(((c.northing - (slopeUser * c.easting))/(slopeperp - slopeUser)))
            yperp.append(slopeperp*((c.northing - (slopeUser*c.easting))/(slopeperp - slopeUser)))
        }
        // iii. Declare max and min variables to be used in calculation as starting point
        let xMax = xperp.max()
        let xMin = xperp.min()
        let yMax = yperp.max()
        let yMin = yperp.min()
        
        // iv. Calcualte n, number of waypoint lines.
        let distperp = pow((pow((xMax!-xMin!),2)+pow((yMax!-yMin!),2)), 0.5);
        xperp.removeAll();
        yperp.removeAll();
        if (distperp<shift) {
            //don't run the code
        }
        else {
            //Run the WP algorithm on inputs and points.
            var wp = WP(points: points, sidelap: sidelap, polydegreeUser: polydegreeUser, altitude: altitude);
            //Convert the WPs from UTM back to WSG
            var i = 0
            while (i < wp.count - 1) {
                
                let wpUTM = UTMCoordinatesMake(wp[i+1], wp[i], zoner, lett)
                let wplatlng = converter.utmCoordinates(toLatitudeAndLongitude: wpUTM)
                //Store each wp as a list
                wpalgorithmlist.append(wplatlng);
                i += 2
        
            }
        }
        return wpalgorithmlist;
    }
    
    //Parallel Flight Algorithm
//    func parallel (numdrones: Int,  OrderedPoints: Array<Double>,  slopeUser: Double) -> Array<Array<Double>> {
//        ArrayList<Double> OrderedPointsx = splitxfun(OrderedPoints);
//        ArrayList<Double> OrderedPointsy = splityfun(OrderedPoints);
//        //Initialize parallel array
//        var parallelout = Array<Array<Double>>()
//        // for each drone
//        var i = 0
//        while (i<numdrones){
//            ArrayList<Double> WPfile = new ArrayList<>();
//            if (i==0){ // Add first point to first drone
//                WPfile.add(OrderedPointsx.get(0));
//                WPfile.add(OrderedPointsy.get(0));
//            }
//            //Declare boolean for every other iteration
//            boolean parallelswitch = false;
//            // for every two sets of x,y points
//            for (int j=((2*i)+1); j < OrderedPointsx.size()-1; j+=2*numdrones){
//                if ((slopeUser >= 0) && (slopeUser < 90)) { //positive slope
//                    if (!parallelswitch) {
//                        // min and max are the same for x and y
//                        WPfile.add(Math.min(OrderedPointsx.get(j), OrderedPointsx.get(j + 1)));
//                        WPfile.add(Math.min(OrderedPointsy.get(j), OrderedPointsy.get(j + 1)));
//                        WPfile.add(Math.max(OrderedPointsx.get(j), OrderedPointsx.get(j + 1)));
//                        WPfile.add(Math.max(OrderedPointsy.get(j), OrderedPointsy.get(j + 1)));
//                        parallelswitch = true;
//                    }
//                    else {
//                        WPfile.add(Math.max(OrderedPointsx.get(j), OrderedPointsx.get(j + 1)));
//                        WPfile.add(Math.max(OrderedPointsy.get(j), OrderedPointsy.get(j + 1)));
//                        WPfile.add(Math.min(OrderedPointsx.get(j), OrderedPointsx.get(j + 1)));
//                        WPfile.add(Math.min(OrderedPointsy.get(j), OrderedPointsy.get(j + 1)));
//                        parallelswitch = false;
//                    }
//                }
//                    if (slopeUser >= 90) { //negative slope
//                        if (!parallelswitch) {
//                        // min and max are the same for x and y
//                        WPfile.add(Math.min(OrderedPointsx.get(j), OrderedPointsx.get(j + 1)));
//                        WPfile.add(Math.max(OrderedPointsy.get(j), OrderedPointsy.get(j + 1)));
//                        WPfile.add(Math.max(OrderedPointsx.get(j), OrderedPointsx.get(j + 1)));
//                        WPfile.add(Math.min(OrderedPointsy.get(j), OrderedPointsy.get(j + 1)));
//                        parallelswitch = true;
//                        }
//                    else {
//                        WPfile.add(Math.max(OrderedPointsx.get(j), OrderedPointsx.get(j + 1)));
//                        WPfile.add(Math.min(OrderedPointsy.get(j), OrderedPointsy.get(j + 1)));
//                        WPfile.add(Math.min(OrderedPointsx.get(j), OrderedPointsx.get(j + 1)));
//                        WPfile.add(Math.max(OrderedPointsy.get(j), OrderedPointsy.get(j + 1)));
//                        parallelswitch = false;
//                    }
//                }
//            }
//    
//            parallelout.add(WPfile);
//            i+=1
//        } // for loop
//    
//        return(parallelout);
//    } // End parallel method

    func toRadians(degrees: Double) -> Double{
        return (Double.pi/180.0)*degrees
    }
    func slopefun(points: Array<UTMCoordinates>) -> Array<Double> {
        //return array of the slope between each consecutive point
        var polyslope = [Double]()
        for (i,c) in points.enumerated(){
            if(i == points.count-1){
                polyslope.append((points[0].easting - c.easting)/(points[0].northing - c.northing))
            }else{
                polyslope.append((points[i+1].easting - c.easting)/(points[i+1].northing - c.northing))
            }
        }
        return polyslope
    }
    func intfun(points: Array<UTMCoordinates>) -> Array<Double> {
        //return the intercept between each consecutive point
        var polyint = [Double]()
        for (i,c) in points.enumerated(){
            if(i == points.count-1){
                polyint.append(points[0].easting - (points[0].easting - c.easting)/(points[0].northing - c.northing))
            }else{
                polyint.append(c.easting - (points[i+1].easting - c.easting)/(points[i+1].northing - c.northing))
            }
        }
        return polyint
    }
    func polyminfun(points: Array<Double>) -> Array<Double> {
        //find minimum value between each consecutive value in the inputted array
        var polymin = [Double]()
        for (i,c) in points.enumerated(){
            if(i == points.count-1){
                polymin.append(min(points[0], c))
            }else{
                polymin.append(min(c, points[i+1]))
            }
        }
        return polymin
    }
    func polymaxfun(points: Array<Double>) -> Array<Double> {
        //find max value between each consecutive value in the inputted array
        var polymin = [Double]()
        for (i,c) in points.enumerated(){
            if(i == points.count-1){
                polymin.append(max(points[0], c))
            }else{
                polymin.append(max(c, points[i+1]))
            }
        }
        return polymin
    }

    func shiftfun(width: Double, sidelap: Double) -> Double {
        return (width * (1-sidelap))
    }
    func wsgListToUTM(points: Array<CLLocationCoordinate2D>) -> Array<UTMCoordinates>{
        var utmList = [UTMCoordinates]()
        for x in points{
            utmList.append(converter.latitudeAndLongitude(toUTMCoordinates: x))
        }
        return utmList
    }
    func utmListToWSG(points: Array<UTMCoordinates>) -> Array<CLLocationCoordinate2D>{
        var utmList = [CLLocationCoordinate2D]()
        for x in points{
            utmList.append(converter.utmCoordinates(toLatitudeAndLongitude: x))
        }
        return utmList
    }
    func gernfun(oaymax: Double, oaymin: Double, width: Double, sidelap: Double) -> Int{
        let distperp = oaymax - oaymin
        let n = distperp / (width * (1-sidelap))
        let gern =  Int(ceil(n))
        return gern
    }
    /**
     * Calculate the area of a polygon.
     * http://www.mathopenref.com/coordpolygonarea2.html
     */
    func polygonarea(polylist: Array<CLLocationCoordinate2D>) -> Double{
        var utmCoords = wsgListToUTM(points: polylist)
        var j = polylist.count - 1
        var area = 0.0
        for (i,c) in utmCoords.enumerated(){
            area = area + ((utmCoords[j].easting+c.easting)*(utmCoords[j].northing-c.northing))
            j=i //j is previous vertex to i
        }
        let areaH = area/20000 //in hecatres
        let areai = abs(round(areaH*1000))
        return areai/1000 //final answer in hectares
    }
    
    func dronedistfun(wpalgorithmlist: Array<CLLocationCoordinate2D>) -> Double{
        var utmCoords = wsgListToUTM(points: wpalgorithmlist)
        var dronedist = 0.0
        var i = 0
        let count = utmCoords.count
        while i < count-1{
            dronedist = dronedist + pow((pow((utmCoords[i+1].easting-utmCoords[i].easting),2) + pow(utmCoords[i+1].northing-utmCoords[i].northing,2)),0.5)
            dronedist = dronedist + pow((pow((utmCoords[1].easting-utmCoords[count-1].easting),2) + pow((utmCoords[1].northing-utmCoords[count-1].northing),2)),0.5)
            i+=2
        }
        return round(dronedist) //final answer in meters
    }
    
    func WP(points: Array<UTMCoordinates>, sidelap: Double, polydegreeUser: Double, altitude: Double) -> Array<Double>{
        var pointsx = [Double]()
        var pointsy = [Double]()
        for c in points{
            pointsx.append(c.easting)
            pointsy.append(c.northing)
        }
        
        // 1. Determine slope and intercept of boundary lines.
        var polyslope = slopefun(points: points)
        var polyint = intfun(points: points)
        
        // 2. Determine each boundary line's y-coordinate and x-coordinate min & max for the purpose of bounding points.
        var ymin = polyminfun(points: pointsy)
        var ymax = polymaxfun(points: pointsy)
        var xmin = polyminfun(points: pointsx)
        var xmax = polymaxfun(points: pointsx)
        
        // 3. Determine area (height and width) that camera (based on drone type) can capture at any given altitude.
        let width = altitude * 1.71111111122

        // 4. Declare arrays for finding x and y coordinates for waypoints.
        var x = [Double]()
        var y = [Double]()
        
        // 5. The remaining code is used to determine x and y coordinates for waypoints depending on the user inputs
        var OrderedPointsx = [Double]()
        var OrderedPointsy = [Double]()
        
        if(polydegreeUser == 90){
            
            //Define overall x min and x max of boundary points for determining number of waypoint lines
            let oaxmin = pointsx.min()
            let oaxmax = pointsx.max()
            
            //Calculate n, total number of waypoint lines
            let gern = gernfun(oaymax: oaxmax!, oaymin: oaxmin!, width: width, sidelap: sidelap)
            
            //Determine distance of y intercepts between waypoint lines
            let shift = shiftfun(width: width, sidelap: sidelap)
            
            //for loop to determine intersection points between boundary lines and waypoint lines
            var j = 0
            while (j < gern){
                for (i,_) in pointsx.enumerated(){
                    let intersy = polyslope[i]*(oaxmin!+(Double(j)*shift))+polyint[i]
                    let intersx = ((polyslope[i]*(oaxmin!+(Double(j)*shift))+polyint[i]) - polyint[i])/polyslope[i]
                    
                    // Bound points by predefined xmax and xmin
                    if (intersx >= xmin[i] - 0.01 && intersx <= xmax[i]+0.01 && intersy >= ymin[i]-0.01 && intersy <= ymax[i]+0.01){
                        y.append(intersy)
                        y.append(Double(j))
                        x.append(intersx)
                        x.append(Double(j))
                    }
                }
                j += 1
            }
            //Order waypoints 
            //Determine indicies of last intersection point for each waypoint line
            //Declare array to hold indices
            var arrofI = [Int?](repeating: nil, count: gern-1)
            //Set initial mostRecentISeen to the first point
            var mostRecentISeen = Int(round(y[1]))
            //Determine indicies, where each index represents the last intersection on each waypoint line
            var i = 3
            while (i < y.count){
                if(y[i] != Double(mostRecentISeen)){
                    arrofI[mostRecentISeen] = i-2
                    mostRecentISeen = Int(round(y[i]))
                }
                i += 2
            }
            //Determine max and min for each waypoint line
            // Store intersection points for first waypoint line
            var bigorsmally = [Double]()
            var bigorsmallx = [Double]()
            i = 1
            while(i < y.count){
                if(i<=arrofI[0]!){
                    bigorsmally.append(y[i-1])
                    bigorsmallx.append(x[i-1])
                }
                i+=2
            }
            
            //Store max then min of intersection points from the first waypoint line in new array, OrderedPoints
            OrderedPointsy.append(bigorsmally.max()!)
            OrderedPointsy.append(bigorsmally.min()!)
            var iymax = bigorsmally.index(of: bigorsmally.max()!)
            var iymin = bigorsmally.index(of: bigorsmally.min()!)
            OrderedPointsx.append(bigorsmallx[iymax!])
            OrderedPointsx.append(bigorsmallx[iymin!])
            
            //Store intersection points of 2nd:2nd to last waypoint lines
            var bigorsmallx2 = [Double]()
            var bigorsmally2 = [Double]()
            j = 0
            while(j<arrofI.count-1){
                i=1
                while(i<y.count){
                    if(i>arrofI[j]! && i<=arrofI[j+1]!){
                        bigorsmally2.append(y[i-1])
                        bigorsmallx2.append(x[i-1])
                    }
                    i+=2
                }
                
                //Store max and min of 2nd:2nd to last waypoint lines in array, OrderedPoints
                OrderedPointsy.append(bigorsmally2.max()!)
                OrderedPointsy.append(bigorsmally2.min()!)
                iymax = bigorsmally2.index(of: bigorsmally2.max()!)
                iymin = bigorsmally2.index(of: bigorsmally2.min()!)
                OrderedPointsx.append(bigorsmallx2[iymax!])
                OrderedPointsx.append(bigorsmallx2[iymin!])
                
                //Clear the arrays each time in order to only include values only from waypoint lines evaluated for max and min
                bigorsmallx2.removeAll()
                bigorsmally2.removeAll()
                j+=1
            }//end for loop for storing intersection points
            
            //store intersection points for last waypoint line
            i=1
            while(i < y.count){
                if(i>arrofI[arrofI.count-1]!){
                    bigorsmally2.append(y[i-1])
                    bigorsmallx2.append(x[i-1])
                }
                i+=2
            }
            
            //Store max and min of the last waypoint lines in array, OrderedPoints
            OrderedPointsy.append(bigorsmally2.max()!)
            OrderedPointsy.append(bigorsmally2.min()!)
            iymax = bigorsmally2.index(of: bigorsmally2.max()!)
            iymin = bigorsmally2.index(of: bigorsmally2.min()!)
            OrderedPointsx.append(bigorsmallx2[iymax!])
            OrderedPointsx.append(bigorsmallx2[iymin!])
            
            //Swap every other x point to ensure drone flies max --> min then min --> max
            i=2
            while(i < OrderedPointsy.count-1){
                swap(&OrderedPointsy[i], &OrderedPointsy[i+1])
                swap(&OrderedPointsx[i], &OrderedPointsx[i+1])
                i+=4
            }
            
        }else if(polydegreeUser == 0){
            // Define overall y min and y max of boundary points for determining number of waypoint lines.
            let oaymin = pointsy.min()
            let oaymax = pointsy.max()
            
            //Calculate n, total number of waypoint lines
            let gern = gernfun(oaymax: oaymax!, oaymin: oaymin!, width: width, sidelap: sidelap)
            
            //Determine distance of y intercepts between waypoint lines
            let shift = shiftfun(width: width, sidelap: sidelap)
            
            //for loop to determine intersection points between boundary lines and waypoint lines
            var j = 0
            while (j < gern){
                for (i,_) in pointsx.enumerated(){
                    var intersx = (oaymin! + (Double(j)*shift) - pointsy[i] + (polyslope[i]*pointsx[i]))
                    intersx = intersx/polyslope[i]
                    //temp var to breakup expression
                    let temp = (oaymin! + Double(j)*shift-pointsy[i]+polyslope[i]*pointsx[i])
                    let intersy = polyslope[i]*((temp/polyslope[i])-pointsx[i])+pointsy[i]
                    
                    // Bound points by predefined xmax and xmin
                    if (intersx >= xmin[i] - 0.01 && intersx <= xmax[i]+0.01 && intersy >= ymin[i]-0.01 && intersy <= ymax[i]+0.01){
                        x.append(intersy)
                        x.append(Double(j))
                        x.append(intersx)
                        x.append(Double(j))
                    }
                }
                j += 1
            }
            //Order waypoints
            //Determine indicies of last intersection point for each waypoint line
            //Declare array to hold indices
            var arrofI = [Int?](repeating: nil, count: gern-1)
            //Set initial mostRecentISeen to the first point
            var mostRecentISeen = Int(round(x[1]))
            //Determine indicies, where each index represents the last intersection on each waypoint line
            var i = 3
            while (i < x.count){
                if(x[i] != Double(mostRecentISeen)){
                    arrofI[mostRecentISeen] = i-2
                    mostRecentISeen = Int(round(x[i]))
                }
                i += 2
            }
            //Determine max and min for each waypoint line
            // Store intersection points for first waypoint line
            var bigorsmallx = [Double]()
            var bigorsmally = [Double]()
            i = 1
            while(i < x.count){
                if(i<=arrofI[0]!){
                    bigorsmallx.append(x[i-1])
                    bigorsmally.append(y[i-1])
                }
                i+=2
            }
            
            //Store max then min of intersection points from the first waypoint line in new array, OrderedPoints
            OrderedPointsx.append(bigorsmallx.max()!)
            OrderedPointsx.append(bigorsmallx.min()!)
            var ixmax = bigorsmally.index(of: bigorsmallx.max()!)
            var ixmin = bigorsmally.index(of: bigorsmallx.min()!)
            OrderedPointsy.append(bigorsmallx[ixmax!])
            OrderedPointsy.append(bigorsmallx[ixmin!])
            
            //Store intersection points of 2nd:2nd to last waypoint lines
            var bigorsmallx2 = [Double]()
            var bigorsmally2 = [Double]()
            j = 0
            while(j<arrofI.count-1){
                i=1
                while(i<x.count){
                    if(i>arrofI[j]! && i<=arrofI[j+1]!){
                        bigorsmallx2.append(x[i-1])
                        bigorsmally2.append(x[i-1])
                    }
                    i+=2
                }
                
                //Store max and min of 2nd:2nd to last waypoint lines in array, OrderedPoints
                OrderedPointsx.append(bigorsmallx2.max()!)
                OrderedPointsx.append(bigorsmallx2.min()!)
                ixmax = bigorsmallx2.index(of: bigorsmallx2.max()!)
                ixmin = bigorsmallx2.index(of: bigorsmallx2.min()!)
                OrderedPointsy.append(bigorsmally2[ixmax!])
                OrderedPointsy.append(bigorsmally2[ixmin!])
                
                //Clear the arrays each time in order to only include values only from waypoint lines evaluated for max and min
                bigorsmallx2.removeAll()
                bigorsmally2.removeAll()
                j+=1
            }//end for loop for storing intersection points
            
            //store intersection points for last waypoint line
            i=1
            while(i < x.count){
                if(i>arrofI[arrofI.count-1]!){
                    bigorsmallx2.append(x[i-1])
                    bigorsmally2.append(y[i-1])
                }
                i+=2
            }
            
            //Store max and min of the last waypoint lines in array, OrderedPoints
            OrderedPointsx.append(bigorsmallx2.max()!)
            OrderedPointsx.append(bigorsmallx2.min()!)
            ixmax = bigorsmallx2.index(of: bigorsmallx2.max()!)
            ixmin = bigorsmallx2.index(of: bigorsmallx2.min()!)
            OrderedPointsy.append(bigorsmally2[ixmax!])
            OrderedPointsy.append(bigorsmally2[ixmin!])
            
            //Swap every other x point to ensure drone flies max --> min then min --> max
            i=2
            while(i < OrderedPointsx.count-1){
                swap(&OrderedPointsx[i], &OrderedPointsx[i+1])
                swap(&OrderedPointsy[i], &OrderedPointsy[i+1])
                i+=4
            }

        }else{
            // Determine user slope from degrees input.
            let slopeUser = tan(toRadians(degrees: polydegreeUser));
            
            // Calcualte n, total number of waypoint lines.
            
            // i. Determine slope perpendicular to slopeUser
            let slopeperp = tan(toRadians(degrees: polydegreeUser+90));
            
            // ii. Determine intersection points made from boundary points
            // with slopeUser and a line with slope perpendicular to slope User.
            var xperp = [Double]()
            var yperp = [Double]()
            var i = 0
            while (i < pointsx.count){
                xperp.append((pointsy[i] - (slopeUser * pointsx[i])) / (slopeperp - slopeUser))
                yperp.append(slopeperp * ((pointsy[i] - (slopeUser * pointsx[i])) / (slopeperp - slopeUser)))
                i += 1
            }
            // iii. Declare max and min variables to be used in calculation as starting point
            let xMax = xperp.max();
            let xMin = xperp.min();
            let yMax = yperp.max();
            let yMin = yperp.min();
            
            // iv. Calcualte n, number of waypoint lines.
            let distperp = pow((pow(xMax!-xMin!,2)+pow(yMax!-yMin!,2)), 0.5);
            let n = distperp / (width*(1-sidelap));
            let gern = ceil(n);
            
            // Determine distance of y intercepts between waypoint lines.
            var shift = 0.0;
            if (slopeperp>0){
                shift = (width*(1-sidelap)) / (sin(toRadians(degrees: polydegreeUser-90)));
            }
            if (slopeperp<0){
                shift = (width*(1-sidelap)) / (sin(toRadians(degrees: 90-polydegreeUser)));
            }
            // for loop to determine intersection points between boundary lines and waypoint lines.
            var j = 0
            while (Double(j) < gern){ // Waypoint lines
                for (i,_) in pointsx.enumerated() {   // Boundary lines
                    let indexj = Double(j)
                    var temp = -polyint[i] + yMin! - (slopeUser * xMax!)
                    temp = temp  + indexj * shift
                    let intersxn = temp / (polyslope[i] - slopeUser);
                    temp = polyslope[i] * pointsx[i] - pointsy[i]
                    temp = temp - slopeUser * xMax! + yMin! + indexj * shift
                    let intersyn = polyslope[i] * ((temp / (polyslope[i] - slopeUser)) - pointsx[i]) + pointsy[i]
                    temp = polyslope[i] * pointsx[i] - pointsy[i]
                    temp = temp - slopeUser * xMin! + yMin! + indexj * shift
                    let intersxp = (temp) / (polyslope[i] - slopeUser)
                    temp = polyslope[i] * pointsx[i] - pointsy[i]
                    temp = temp - slopeUser * xMin! + yMin! + indexj * shift
                    let intersyp = polyslope[i] * (((temp) / (polyslope[i] - slopeUser)) - pointsx[i]) + pointsy[i]
                    // Bound points by predefined x/ymax and x/ymin.
                    if (slopeperp < 0){
                        if (intersxn >= xmin[i] - 0.01 && intersxn <= xmax[i] + 0.01 && intersyn >= ymin[i] - 0.01 && intersyn <= ymax[i] + 0.01){
                            x.append(intersxn);
                            x.append(indexj);
                            y.append(intersyn);
                            y.append(indexj);
                        }
                    }
                    
                    
                    // Bound points by predefined xmax and xmin.
                    if (slopeperp > 0){
                        if (intersxp >= xmin[i]-0.01 && intersxp <= xmax[i]+0.01 && intersyp >= ymin[i]-0.01 && intersyp <= ymax[i]+0.01){
                            x.append(intersxp);
                            x.append(indexj);
                            y.append(intersyp);
                            y.append(indexj);
                        }
                    }
                    
                } // End boundary line for loop
                j += 1
            } // End waypoint line for loop
            
            // Order Waypoints
            
            // Determine indicies of last intersection point for each way point line.
            
            // Declare array to hold indicies.
            var arrofI = [Int?](repeating: nil, count: Int(gern) - 1)
            // Set initial mostRecentISeen to the first point.
            var mostRecentISeen = Int(round(x[1]))
            // Determine indicies, where each index represents the last intersection on each waypoint line.
            i = 3
            while (i < x.count ){
                if (x[i] != Double(mostRecentISeen)){
                    arrofI[mostRecentISeen] = i-2
                    mostRecentISeen = Int(round(x[i]))
                }
                i += 2
            }
            
            // Determine max and min for each waypoint line.
            // Store intersection points for first waypoint line.
            var bigorsmallx = [Double]()
            var bigorsmally = [Double]()
            i = 1
            while (i < x.count ){
                if (i <= arrofI[0]!){
                    bigorsmallx.append(x[i-1]);
                    bigorsmally.append(y[i-1]);
                }
                i += 2
            }
            
            // Store max then min of intersection points from the first waypoint line in new array, OrderedPoints.
            OrderedPointsx.append(bigorsmallx.max()!)
            OrderedPointsx.append(bigorsmallx.min()!)
            var ixmax = bigorsmallx.index(of: bigorsmallx.max()!)
            var ixmin = bigorsmallx.index(of: bigorsmallx.min()!)
            OrderedPointsy.append(bigorsmally[ixmax!])
            OrderedPointsy.append(bigorsmally[ixmin!])
            
            // Store intersection points of 2nd:2nd to last waypoint lines.
            var bigorsmallx2 = [Double]();
            var bigorsmally2 = [Double]();
            j = 0
            while (j < arrofI.count-1){
                i = 1
                while (i < x.count){
                    if ((i>arrofI[j]!) && (i<=arrofI[j+1]!)){
                        bigorsmallx2.append(x[i-1]);
                        bigorsmally2.append(y[i-1]);
                    }
                    i += 2
                }
                
                // Store max and min of 2nd:2nd to last waypoint lines in array, OrderedPoints.
                OrderedPointsx.append(bigorsmallx2.max()!)
                OrderedPointsx.append(bigorsmallx2.min()!)
                ixmax = bigorsmallx2.index(of: bigorsmallx2.max()!)
                ixmin = bigorsmallx2.index(of: bigorsmallx2.min()!)
                OrderedPointsy.append(bigorsmally2[ixmax!])
                OrderedPointsy.append(bigorsmally2[ixmin!])
                
                bigorsmallx2.removeAll(); // Clear the array each time so that only values from each waypoint line are evaluated for max and min.
                bigorsmally2.removeAll();
                j += 1
            } // End for loop for storing intersection points.
            
            // Store intersection points for last waypoint line.
            var bigorsmallx3 = [Double]();
            var bigorsmally3 = [Double]();
            i = 1
            while (i < x.count){
                if (i > arrofI[arrofI.count-1]!){
                    bigorsmallx3.append(x[i-1]);
                    bigorsmally3.append(y[i-1]);
                }
                i += 2
            }
            
            // Store max and min of the last waypoint lines in array, OrderedPoints.
            OrderedPointsx.append(bigorsmallx3.max()!);
            OrderedPointsx.append(bigorsmallx3.min()!);
            ixmax = bigorsmallx3.index(of: bigorsmallx3.max()!);
            ixmin = bigorsmallx3.index(of: bigorsmallx3.min()!);
            OrderedPointsy.append(bigorsmally3[ixmax!]);
            OrderedPointsy.append(bigorsmally3[ixmin!]);
            
            // Swap every other x point to ensure drone flies max--> min then min--> max.
            i = 2
            while (i < OrderedPointsx.count-1){
                swap(&OrderedPointsx[i], &OrderedPointsx[i+1])
                swap(&OrderedPointsy[i], &OrderedPointsy[i+1])

                i += 4
            }

        }//end else
        
        // Convert OrderedPointsx and OrderedPointsy to a 2d array
        var OrderedPoints1 = Array<Double>();
        var i = 0
        while (i < OrderedPointsx.count){
            OrderedPoints1.append(OrderedPointsx[i])
            OrderedPoints1.append(OrderedPointsy[i])
            i += 1
        }
        //Remove any points that are less than 15 meters apart.
        i=0
        while (i < OrderedPoints1.count-3 ){
            let dist = pow((pow((OrderedPoints1[i]-OrderedPoints1[i+2]),2)+pow((OrderedPoints1[i+1]-OrderedPoints1[i+3]),2)), 0.5)
            if (dist<5){
                OrderedPoints1.remove(at: i)
                //This is really deleting i and then i+1, but since the index updates, we just remove i twice.
                OrderedPoints1.remove(at: i);
                i=0
            }else{
                i += 2
            }

        }
        
    
        
        return OrderedPoints1

    }

}

