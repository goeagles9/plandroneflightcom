//
//  Map+CoreDataProperties.swift
//  
//
//  Created by Richard Demeny on 12/04/2017.
//
//  This file was automatically generated and should not be edited.
//

import Foundation
import CoreData


extension Map {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Map> {
        return NSFetchRequest<Map>(entityName: "Map");
    }

    @NSManaged public var name: String?
    @NSManaged public var coordinates: NSSet?

}

// MARK: Generated accessors for coordinates
extension Map {

    @objc(addCoordinatesObject:)
    @NSManaged public func addToCoordinates(_ value: Coordinate)

    @objc(removeCoordinatesObject:)
    @NSManaged public func removeFromCoordinates(_ value: Coordinate)

    @objc(addCoordinates:)
    @NSManaged public func addToCoordinates(_ values: NSSet)

    @objc(removeCoordinates:)
    @NSManaged public func removeFromCoordinates(_ values: NSSet)

}
