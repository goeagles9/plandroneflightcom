//
//  Login.swift
//  FlightPlanning
//
//  Created by Richard Demeny on 21/04/2017.
//  Copyright © 2017 Nimbus. All rights reserved.
//

import UIKit

class Login: UIViewController {
    @IBOutlet var userTextField: UITextField!
    @IBOutlet var passwordTextField: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func notAMemberYetButton(_ sender: Any) {
        self.performSegue(withIdentifier: "NotAMemberYet", sender: nil)
    }
    
    @IBAction func LoginButtonClicked(_ sender: Any) {
        PFUser.logInWithUsername(inBackground: userTextField.text!, password: passwordTextField.text!) { user, error in
            if error != nil {
                self.performSegue(withIdentifier: "loginSucceeded", sender: nil)
            } else if let error = error {
                //    self.showErrorView(error)
            }
        }

    }
 
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
