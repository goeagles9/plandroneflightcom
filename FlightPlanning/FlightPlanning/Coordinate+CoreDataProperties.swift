//
//  Coordinate+CoreDataProperties.swift
//  
//
//  Created by Richard Demeny on 12/04/2017.
//
//  This file was automatically generated and should not be edited.
//

import Foundation
import CoreData


extension Coordinate {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Coordinate> {
        return NSFetchRequest<Coordinate>(entityName: "Coordinate");
    }

    @NSManaged public var latitude: Double
    @NSManaged public var longitude: Double
    @NSManaged public var map: Map?

}
