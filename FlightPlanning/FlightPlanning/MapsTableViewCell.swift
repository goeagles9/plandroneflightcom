//
//  MapsTableViewCell.swift
//  FlightPlanning
//
//  Created by Richard Demeny on 11/04/2017.
//  Copyright © 2017 Nimbus. All rights reserved.
//

import Foundation
import UIKit

class MapsTableViewCell: UITableViewCell {
    
    // MARK: - Properties
    
    static let reuseIdentifier = "MapsCell"
    
    // MARK: -
    
    @IBOutlet var mapNameLabel: UILabel!
    @IBOutlet var deleteMapButton: UIButton!
    @IBOutlet var loadMapButton: UIButton!
    @IBOutlet var sendMapButton: UIButton!


    // MARK: - Initialization
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
}
