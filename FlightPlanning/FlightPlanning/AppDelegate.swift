//
//  AppDelegate.swift
//  FlightPlanning
//
//  Created by Richard Demeny on 02/04/2017.
//  Copyright © 2017 Nimbus. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import Swift
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UISplitViewControllerDelegate {
    var window: UIWindow?
    var productCommunicationManager = ProductCommunicationManager()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        GMSServices.provideAPIKey("AIzaSyCvjSyhp1TuORmHBaaJsRbAMrAv0qfrwu8")
        GMSPlacesClient.provideAPIKey("AIzaSyAkefRgwSvdAQG-OanyI5-MZlha43BcZzA")
        self.productCommunicationManager.registerWithSDK()
        let parseConfiguration = ParseClientConfiguration(block: { (ParseMutableClientConfiguration) -> Void in
            ParseMutableClientConfiguration.applicationId = "nimbus"
            ParseMutableClientConfiguration.clientKey = "CLIENT_KEY"
            ParseMutableClientConfiguration.server = "http://nimbusflight.io:5020/parse"
        })
        Parse.initialize(with: parseConfiguration)
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        self.saveContext()
    }
    
    func saveContext () {
        var error: NSError? = nil
        if managedObjectContext != nil {
            if managedObjectContext.hasChanges{
                do {
                    try managedObjectContext.save()
                } catch {
                    print("Unable to save: \(error)")
                    abort()
                }
            } else {
                print("Unable to save: \(error)")
                abort()
            }
            }
        }
}

//MARK: - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
var managedObjectContext: NSManagedObjectContext {
    if !(_managedObjectContext != nil) {
        let coordinator = persistentStoreCoordinator
        if coordinator != nil {
            _managedObjectContext = NSManagedObjectContext()
            _managedObjectContext!.persistentStoreCoordinator = coordinator
        }
    }
    return _managedObjectContext!
}
var _managedObjectContext: NSManagedObjectContext? = nil

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
var managedObjectModel: NSManagedObjectModel {
    if !(_managedObjectModel != nil) {
        let modelURL = Bundle.main.url(forResource: "Model", withExtension: "momd")
        _managedObjectModel = NSManagedObjectModel(contentsOf: modelURL!)
    }
    return _managedObjectModel!
}
var _managedObjectModel: NSManagedObjectModel? = nil

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
var persistentStoreCoordinator: NSPersistentStoreCoordinator {
    if !(_persistentStoreCoordinator != nil) {
        let storeURL = applicationDocumentsDirectory.appendingPathComponent("Model.sqlite")
        var error: NSError? = nil
        _persistentStoreCoordinator = NSPersistentStoreCoordinator(managedObjectModel: managedObjectModel)
        do {
            try persistentStoreCoordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: storeURL, options: nil)
        } catch let error as NSError {
            print(error.localizedDescription)
        }
    }
    return _persistentStoreCoordinator!
}
var _persistentStoreCoordinator: NSPersistentStoreCoordinator? = nil

// MARK: - Application's Documents directory

// Returns the URL to the application's Documents directory.
var applicationDocumentsDirectory: NSURL {
    let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
    return urls[urls.endIndex-1] as NSURL
}
