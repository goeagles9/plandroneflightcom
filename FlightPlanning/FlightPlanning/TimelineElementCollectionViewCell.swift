//
//  TimelineElementCollectionViewCell.swift
//  FlightPlanning
//
//  Created by Richard Demeny on 02/04/2017.
//  Copyright © 2017 Nimbus. All rights reserved.
//

import UIKit
import Swift

class TimelineElementCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var label: UILabel!
}
